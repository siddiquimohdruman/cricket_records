from django.db import models

# Create your models here.


class Team(models.Model):
    """model for Team"""
    name = models.CharField(max_length=65)
    logoUri = models.CharField(max_length=255, verbose_name='Team Logo')
    club_state = models.CharField(max_length=255, unique=True, verbose_name='Club or State')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')

    class Meta:
        verbose_name = "Team"
        verbose_name_plural = "Team"
        db_table = "team"

    def __str__(self):
        return str(self.name)


class Player(models.Model):
    """model for Player"""      
    SPECIALITY = [('batsman', 'BATSMAN'),('bowler', 'BOWLER'),('all rounder', 'ALL ROUNDER')]
    
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=65, verbose_name='First Name')
    last_name = models.CharField(max_length=65, verbose_name='Last Name')
    imageUri = models.CharField(max_length =255 , verbose_name='Player Image')
    is_captain = models.BooleanField(default=False,verbose_name='Captain')
    player_jersey_number = models.IntegerField(verbose_name='Player Jersey Number')
    country = models.CharField(max_length=65, verbose_name='Country')
    speciality = models.CharField(max_length = 30, choices= SPECIALITY)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')

    class Meta:
        verbose_name = "Player"
        verbose_name_plural = "Player"
        db_table = "player"

    def __str__(self):
        return str(self.first_name+" "+self.last_name)



class PlayerRecord(models.Model):
    """model for Player"""      
    player = models.ForeignKey(Player, on_delete=models.CASCADE,related_name = 'player')
    matches = models.IntegerField(verbose_name='Matches',null=True,blank=True)
    innings = models.IntegerField(verbose_name='Innings',null=True,blank=True)
    runs = models.IntegerField(verbose_name='Runs',null=True,blank=True)
    highest_score = models.IntegerField(verbose_name='Highest Score',null=True,blank=True)
    hundreds = models.IntegerField(verbose_name='100',null=True,blank=True) 
    fifties =  models.IntegerField(verbose_name='50',null=True,blank=True)  
    wickets =  models.IntegerField(verbose_name='wickets',null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')

    class Meta:
        verbose_name = "Player Record"
        verbose_name_plural = "Player Record"
        db_table = "player_record"

    def __str__(self):
        return str(self.player)



class MatchesRecord(models.Model):
    """model for MatchesHistory"""
    OUTPUT = [('draw', 'DRAW'), ('tie', 'TIE'), ('resulted', 'Resulted')]

    team_one = models.ForeignKey(Team, on_delete=models.CASCADE,related_name = 'team_one')
    team_two = models.ForeignKey(Team, on_delete=models.CASCADE,related_name = 'team_two')
    win = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True,related_name = 'win')
    lost = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True,related_name = 'lost')
    result = models.CharField(max_length=30, choices=OUTPUT)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')

    class Meta:
        verbose_name = "Matches Record"
        verbose_name_plural = "Matches Record"
        db_table = "matches_record"

