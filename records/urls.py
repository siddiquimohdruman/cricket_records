from django.urls import path

from .views import (
	TeamList,
	PlayerList,
	PlayerDetails,
	PointTables,
	MatchesFixtures,
	Welcome
)

urlpatterns = [

   path('', Welcome.as_view(), name="team-list"),
   path('team-list/', TeamList.as_view(), name="team-list"),
   path('team-details/<int:id>/',PlayerList.as_view(), name="team-details"),
   path('player-details/<int:player_id>/',PlayerDetails.as_view(), name="player-details"),
   path('points-table/',PointTables.as_view(), name="points-table"),
   path('matches/',MatchesFixtures.as_view(), name="points-table"),
 
]
