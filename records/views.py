from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
# Create your views here.
from django.db.models import Q, Count

from records.models import (
    Team,
    Player,
    PlayerRecord,
    MatchesRecord
)

class Welcome(TemplateView):
    """docstring for Welcome"""
    template_name = 'index.html'

    def get(self, request):

        return render(request, self.template_name, {})
        

class TeamList(TemplateView):
    """docstring for alATeams"""
    template_name = 'teamlist.html'

    def get(self, request):

        team_instance = Team.objects.all().select_related()
        
        return render(request, self.template_name, {'team_instance':team_instance})



class PlayerList(TemplateView):
    """docstring for alATeams"""
    template_name = 'playerlist.html'

    def get(self, request,id):

        player_instance = Player.objects.filter(team=id).select_related('team')
        
        team_name = player_instance.first().team 
        

        return render(request, self.template_name, {
            'all_data':player_instance,
            'team_name':team_name
            })



class PlayerDetails(TemplateView):
    """docstring for alATeams"""
    template_name = 'player_details.html'

    def get(self, request,player_id):

        player_instance = PlayerRecord.objects.filter(player=player_id).select_related('player').first()
        player_img = 'static/team_logo/default_user.jpeg'
        speciality = ''
        if player_instance:
            if player_instance.player:
                player_img = '/static/player_logo/'+ str(player_instance.player.imageUri)
                speciality = player_instance.player.speciality

        return render(request, self.template_name, {
            'data':player_instance,
            'player_img':player_img,
            'speciality':speciality
            })


class PointTables(TemplateView):
    """docstring for alATeams"""
    template_name = 'point_table.html'

    def get(self, request):

        all_teams = Team.objects.all().select_related()
        for team in all_teams:
            match = MatchesRecord.objects.filter(
                Q(team_one=team) | 
                Q(team_two=team)
            ).select_related()
            team.win_count = match.filter(win=team).count()
            team.lost_count = match.filter(lost=team).count()
            team.draw_count = match.filter(Q(result='draw')|Q(result='tie')).count()
            
            team.total_point = team.win_count*2 + team.draw_count*1

            # print('Win',team, team.win_count)    
            # print('draw',team,team.draw_count)    
            # print('lost',team,team.lost_count)    
            # print('totla',team,team.total_point)    
        return render(request, self.template_name, {'all_teams':all_teams})




class MatchesFixtures(TemplateView):
    """docstring for alATeams"""
    template_name = 'match_fixtures.html'

    def get(self, request):


        all_matches = MatchesRecord.objects.all()


        return render(request, self.template_name, {'all_matches':all_matches})


