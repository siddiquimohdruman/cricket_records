import string 
import random,os 
from records.models import Team,Player,MatchesRecord,PlayerRecord
import names
from PIL import Image, ImageDraw,ImageFont
from  cricket_records import settings

def generate_player_img(name):
    file_path = str(settings.BASE_DIR)+'/static/'
    random_string = ''.join(random.choices(string.ascii_uppercase ,k=8))
    image_name = name+str(random_string) +'.png'
    color_list = [(192,192,192),(73, 109, 137),(0,255,0),(255,0,0)]

    img = Image.new('RGB', (100, 100), color = random.choice(color_list))
     
    d = ImageDraw.Draw(img)
    d.text((40,40), name, fill=(0,0,0))

    if not os.path.exists(file_path+'player_logo/'):
        os.mkdir(file_path+'player_logo/')
    final_image_name =image_name
    img.save(file_path+'player_logo/'+final_image_name) 
    return final_image_name

def create_teams():
    
    team_dict = [        
        {
            'name':'Delhi Capitals', 
            'logoUri': '/static/team_logo/dc.png',
            'club_state':'DELHI'
        },
        {
            'name': 'Kolkata knight Riders',
            'logoUri':'/static/team_logo/kkr.jpeg',
            'club_state':'WEST BANGAL'
        },
        {
            'name': 'Mumbai Indians',
            'logoUri':'/static/team_logo/mi.png',
            'club_state':'MAHARASHTRA'
        },
        {
            'name': 'Royal Challengers Bangalore',
            'logoUri':'/static/team_logo/rcb.jpeg',
            'club_state':'KARNATAKA'
        }
    ]
    for data in team_dict:
        instance = Team.objects.get_or_create(**data)
        print('Team : ',instance)


def create_players():
    speciality = ['batsman','bowler','all_rounder']
    all_teams = Team.objects.all()
    all_teams_player = []
    for team in all_teams:        
        for x in range(1,15):
            full_name = names.get_full_name(gender='male').split()
            
            data = {
                'team': team,
                'first_name':full_name[0], 
                'last_name':full_name[1],
                'imageUri': generate_player_img(full_name[0]),
                'player_jersey_number':random.randint(1,100),
                'country':'INDIA',
                'speciality':random.choice(speciality) ,
            }

            instance = Player.objects.get_or_create(**data)

            print('Players :',instance )            

def player_record():
    all_players = Player.objects.all()

    for player in all_players:        
        for x in range(1,15):
            matches = random.randint(1,100)
            innings = random.randint(1,matches)
            runs = random.randint(10,20) * innings

            possible_hundreds = int(runs / 100)  
            hundreds = random.randint(0,possible_hundreds)
            
            possible_fifties =  int(( runs - (hundreds *100)) / 50) 
            fifties = random.randint(0,possible_fifties)

            if innings == 1:
                highest_score = runs
            elif hundreds:
                highest_score = random.randint(100,150)
            elif fifties:
                highest_score = random.randint(50,99)
            else:
                highest_score = random.randint(1,runs)

            possible_wickets = (matches * 2)
            wickets = random.randint(0,possible_wickets)
            data = {
                'player':player,
                'matches':matches, 
                'innings':innings,
                'runs': runs,
                'highest_score':highest_score,
                'hundreds':hundreds,
                'fifties':fifties,
                'wickets':wickets
            }
            instance = PlayerRecord.objects.get_or_create(**data)
            print('Players :',instance )            

def match_record():
    all_teams = Team.objects.all().select_related()
    length = len(all_teams)
    for team in range(length):
        curr_index = team
        next_index = curr_index+1
        for x in range(curr_index, length-1):
            if next_index != length:

                team_one = all_teams[curr_index]
                team_two = all_teams[next_index]
                result_list = ['draw','tie','resulted']
                result = random.choice(result_list)    
                team_list = [team_one,team_two]
                
                if result ==  'resulted':
                    winning_team = random.choice(team_list)  
                    if winning_team == team_one:
                        losing_team = team_two
                    else:
                        losing_team = team_one
                else:
                    winning_team = None         
                    losing_team = None 

                
                data = {
                    'team_one':team_one,
                    'team_two':team_two, 
                    'win':winning_team,
                    'lost': losing_team,
                    'result':result,
                }
                instance = MatchesRecord.objects.get_or_create(**data)
                next_index += 1       

        #for team in all_teams:

        


create_teams()
create_players()
player_record()
match_record()

