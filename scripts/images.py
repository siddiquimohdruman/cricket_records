# import urllib2

from urllib.request import urlopen
import PIL
from PIL import Image, ImageFile 

from records.models import Matchccccc   
from django.core.files import File
# from django.core.files.temp import NamedTemporaryFile
from cStringIO import StringIO
# from io import StringIO
# html = urlopen("http://www.google.com/").read()
# inStream = urllib2.urlopen('http://www.google.com/intl/en_ALL/images/srpr/logo1w.png')
inStream = urlopen('http://www.google.com/intl/en_ALL/images/srpr/logo1w.png')

parser = ImageFile.Parser()
while True:
    s = inStream.read(1024)
    print(s)
    if not s:
        break
    parser.feed(s)

inImage = parser.close()
# convert to RGB to avoid error with png and tiffs
if inImage.mode != "RGB":
    inImage = inImage.convert("RGB")

# resize could occur here

# START OF CODE THAT DOES NOT SEEM TO WORK
# I need to somehow convert an image .....
img_temp = StringIO()
inImage.save(img_temp, 'PNG')
img_temp.seek(0)
file_object = File(img_temp, filename)

# img_temp = NamedTemporaryFile(delete=True)
# img_temp.write(inImage.tostring())
# img_temp.flush()

file_object = File(img_temp)

# .... into a file that the Django object will accept. 
# END OF CODE THAT DOES NOT SEEM TO WORK

Matchccccc.thumbnail.save(
         'some_filename',
         file_object,  # this must be a File() object
         save=True,
         )


class Product(models.Model):
    prod_id = models.CharField('Product ID', max_length=20)
    prod_img = ImageField('Product Image', upload_to='products', null=True, blank=True)
    # upload_to will be a folder within your MEDIA_ROOT, created if it doesn't exist
    # Don't forget to set MEDIA_ROOT and MEDIA_URL in your project's settings.py
    # null=True because not all Products will have an image
    # blank=True because I don't want admin to throw up if I try to save a Product without filling out the ImageField
    # IMPORTANT PART: Below we're going to redefine the save() method for this class. We will essentially say that if save(some_url) is called, go fetch the image at that URL and save it to the ImageField prod_img. (There are possibly other elegant things you can do instead of just passing the image's URL in the save method. You should be able to move around the parts afterward to do what you want.)
    def save(self, url='', *args, **kwargs):
        if self.prod_img != '' and url != '': # Don't do anything if we don't get passed anything!
            image = download_image(url) # See function definition below
            try:
                filename = urlparse.urlparse(url).path.split('/')[-1]
                self.prod_img = filename
                tempfile = image
                tempfile_io = cStringIO.StringIO() # Will make a file-like object in memory that you can then save
                tempfile.save(tempfile_io, format=image.format)
                self.prod_img.save(filename, ContentFile(tempfile_io.getvalue()), save=False) # Set save=False otherwise you will have a looping save method
            except Exception, e:
                print ("Error trying to save model: saving image failed: " + str(e))
                pass
        super(Product, self).save(*args, **kwargs) # We've gotten the image into the ImageField above...now we actually need to save it. We've redefined the save method for Product, so super *should* get the parent of class Product, models.Model and then run IT'S save method, which will save the Product like normal

def download_image(url):
    """Downloads an image and makes sure it's verified.

    Returns a PIL Image if the image is valid, otherwise raises an exception.
    """
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0'} # More likely to get a response if server thinks you're a browser
    r = urllib2.Request(url, headers=headers)
    request = urllib2.urlopen(r, timeout=10)
    image_data = cStringIO.StringIO(request.read()) # StringIO imitates a file, needed for verification step
    img = Image.open(image_data) # Creates an instance of PIL Image class - PIL does the verification of file
    img_copy = copy.copy(img) # Verify the copied image, not original - verification requires you to open the image again after verification, but since we don't have the file saved yet we won't be able to. This is because once we read() urllib2.urlopen we can't access the response again without remaking the request (i.e. downloading the image again). Rather than do that, we duplicate the PIL Image in memory.
    if valid_img(img_copy):
        return img
    else:
        # Maybe this is not the best error handling...you might want to just provide a path to a generic image instead
        raise Exception('An invalid image was detected when attempting to save a Product!')

def valid_img(img):
    """Verifies that an instance of a PIL Image Class is actually an image and returns either True or False."""
    type = img.format
    if type in ('GIF', 'JPEG', 'JPG', 'PNG'):
        try:
            img.verify()
            return True
        except:
            return False
    else: return False